My own CV tests.

Some of the scripts were modified from me taking the [Computer Vision 1: Introduction (Python)](https://opencv.org/courses/).

Most of them were created months even years ago when I didn't write down comprehensible documents.
Today (2022-11-19) I decided to quickly sort them out with simple notes for occasionally visitors and future me.
The scripts are far from complete or clean, just hoping someone may find some useful stuff.

The following mini-projects are listed in alphabetical order.

* [blemish_removal](blemish_removal/): Clicked on the blemish and the script will remove them.

  ![](blemish_removal/pics/result.gif)

* [cartoon_effects](cartoon_effects/): Use bilateral filter and some other stuff to create cartoon effect of the input image or video.
  * You may want to try it with your webcam: `python cartoonify_cam.py`

  ![](cartoon_effects/pics/result.png)

* [chroma_keying](chroma_keying/): Set the background with simple UI.

  ![](chroma_keying/results/res.gif)

* [cv2_dnn](cv2_dnn): Using cv2 DNN module. Messy code and could be outdated.

* <a name="cv_obj_tracking">[cv_obj_tracking](cv_obj_tracking/)</a>: Tests on different tracking methods.
  (Probably similar to [object_trackers](object_trackers)... I just cannot remember why I had both of them...)

  ![](cv_obj_tracking/results/result-20221119.gif)

* [document_scanner](document_scanner/): Not completed...
* [identical_image_checker](identical_image_checker): Tried to come up with a tool to find duplicated images in my own folder.
* [image_alignment_by_feature_matching](image_alignment_by_feature_matching):
  Align B-G-R channels to produce color images.

  ![](image_alignment_by_feature_matching/res.png)

* [interactive_grabcut](interactive_grabcut/): Tried to create a (not-so-user-friendly) 
  [tkinter](https://docs.python.org/3/library/tkinter.html) UI and played with 
  [GrabCut](https://docs.opencv.org/4.5.0/d8/d83/tutorial_py_grabcut.html).

  ![](interactive_grabcut/pics/app_demo-20221119.png)

* [jelly_beans_counter](jelly_beans_counter/): Apply image processing and clustering methods to count numbers of
  [jelly beans](https://sipi.usc.edu/database/database.php?volume=misc&image=8#top).

  ![](jelly_beans_counter/res.png)
  ```
  Output:
  
  Estimated counts: 115, True counts: 82
  
  [0. 0. 0.]: 359
  [72.37958049 36.47586555 47.88728835]: 28
  [ 87.13415038 150.4879099   70.77409738]: 21
  [187.20402958  63.2096404   42.88115277]: 28
  [191.18777661 176.37239199  73.7833509 ]: 34
  ```

* [object_trackers](object_trackers/): See [cv_obj_tracking](#cv_obj_tracking).

* [panorama](panorama/): Stitch multiple images to get a wider panorama one.

  ![](panorama/results/res-20221119.png)

* [reverse_bgr](reverse_bgr/): Encountered an error when using `cv2.rectangle()` and found something about `contiguous`.
  * [cv2.rectangle() TypeError: an integer is required (got type tuple) #14866](https://github.com/opencv/opencv/issues/14866#issuecomment-580207109)
  * [What is the difference between contiguous and non-contiguous arrays?](https://stackoverflow.com/questions/26998223/what-is-the-difference-between-contiguous-and-non-contiguous-arrays)
  * [numpy.ascontiguousarray](https://numpy.org/doc/stable/reference/generated/numpy.ascontiguousarray.html)

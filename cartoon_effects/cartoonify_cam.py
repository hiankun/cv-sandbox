import numpy as np
import cv2
from scripts import cartoon_filters

show_colors = False

def main():
    cap = cv2.VideoCapture(0)
    
    while True:
        _, frame = cap.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
      
        # Sketch
        edges_3ch = cartoon_filters.get_sketch_lines(
            frame,
            blur_ksize=(3,3), 
            laplacian_ksize = 5,
            threshold = (120,255)
        )
        
        if show_colors:
            # Color
            filtered_color = cartoon_filters.get_bilateral(
                frame,
                sigmaColor = 60,
                sigmaSpace = 5 ## large value will slow down the program dramatically
            ) 
          
            res = cv2.bitwise_and(filtered_color, edges_3ch)
        else:
            res = edges_3ch

        cv2.imshow('frame', res)
      
        if cv2.waitKey(33) & 0xFF == ord('q'):
            break
    
    cap.release()
    cv2.destroyAllWindows()
    

if __name__=='__main__':
    main()

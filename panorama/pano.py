import cv2
import numpy as np
import os
from glob import glob
import matplotlib.pyplot as plt


def show_res(imgs, res=None):
    n_imgs = len(imgs)
    fig, axs = plt.subplots(2,n_imgs, figsize=(12,4))

    for i, img in enumerate(imgs):
        axs[0][i].imshow(img)
    for a in axs.flatten():
        a.set_axis_off()

    # put the res img in the bottom row
    gs = axs[1][0].get_gridspec()
    for a in axs[1,:]:
        a.remove()
    axbig = fig.add_subplot(gs[1,:])
    axbig.imshow(res)
    axbig.set_axis_off()

    fig.tight_layout()
    plt.show()
    plt.close()

def main():
    #imagefiles = sorted(glob('./inputs/*.jpg'))
    imagefiles = glob('./inputs/*.jpg')
    
    images = []
    for filename in imagefiles:
      img = cv2.cvtColor(cv2.imread(filename), cv2.COLOR_BGR2RGB)
      images.append(img)
    
    stitcher = cv2.Stitcher.create()
    _, res = stitcher.stitch(images)
    
    #cv2.imshow('Panorama', res[100:-100,50:-50])
    #cv2.waitKey()
    
    show_res(images, res=res)


if __name__=='__main__':
    main()
